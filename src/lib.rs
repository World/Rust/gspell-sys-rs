// This file was generated by gir (https://github.com/gtk-rs/gir @ b5ea065)
// from gir-files (https://github.com/gtk-rs/gir-files @ c49dfc7)
// DO NOT EDIT

#![allow(non_camel_case_types, non_upper_case_globals, non_snake_case)]
#![cfg_attr(feature = "cargo-clippy", allow(approx_constant, type_complexity, unreadable_literal))]

extern crate libc;
extern crate gtk_sys as gtk;
extern crate glib_sys as glib;
extern crate gobject_sys as gobject;

#[allow(unused_imports)]
use libc::{c_int, c_char, c_uchar, c_float, c_uint, c_double,
    c_short, c_ushort, c_long, c_ulong,
    c_void, size_t, ssize_t, intptr_t, uintptr_t, time_t, FILE};

#[allow(unused_imports)]
use glib::{gboolean, gconstpointer, gpointer, GType};

// Enums
pub type GspellCheckerError = c_int;
pub const GSPELL_CHECKER_ERROR_DICTIONARY: GspellCheckerError = 0;
pub const GSPELL_CHECKER_ERROR_NO_LANGUAGE_SET: GspellCheckerError = 1;

// Records
#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellCheckerClass {
    pub parent_class: gobject::GObjectClass,
    pub word_added_to_personal: Option<unsafe extern "C" fn(*mut GspellChecker, *const c_char)>,
    pub word_added_to_session: Option<unsafe extern "C" fn(*mut GspellChecker, *const c_char)>,
    pub session_cleared: Option<unsafe extern "C" fn(*mut GspellChecker)>,
    pub padding: [gpointer; 12],
}

impl ::std::fmt::Debug for GspellCheckerClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellCheckerClass @ {:?}", self as *const _))
         .field("parent_class", &self.parent_class)
         .field("word_added_to_personal", &self.word_added_to_personal)
         .field("word_added_to_session", &self.word_added_to_session)
         .field("session_cleared", &self.session_cleared)
         .field("padding", &self.padding)
         .finish()
    }
}

#[repr(C)]
pub struct GspellCheckerDialogClass {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent_class has incomplete type
}

impl ::std::fmt::Debug for GspellCheckerDialogClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellCheckerDialogClass @ {:?}", self as *const _))
         .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellEntryBufferClass {
    pub parent_class: gobject::GObjectClass,
}

impl ::std::fmt::Debug for GspellEntryBufferClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellEntryBufferClass @ {:?}", self as *const _))
         .field("parent_class", &self.parent_class)
         .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellEntryClass {
    pub parent_class: gobject::GObjectClass,
}

impl ::std::fmt::Debug for GspellEntryClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellEntryClass @ {:?}", self as *const _))
         .field("parent_class", &self.parent_class)
         .finish()
    }
}

#[repr(C)]
pub struct GspellLanguage(c_void);

impl ::std::fmt::Debug for GspellLanguage {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellLanguage @ {:?}", self as *const _))
         .finish()
    }
}

#[repr(C)]
pub struct GspellLanguageChooserButtonClass {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent_class has incomplete type
}

impl ::std::fmt::Debug for GspellLanguageChooserButtonClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellLanguageChooserButtonClass @ {:?}", self as *const _))
         .finish()
    }
}

#[repr(C)]
pub struct GspellLanguageChooserDialogClass {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent_class has incomplete type
}

impl ::std::fmt::Debug for GspellLanguageChooserDialogClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellLanguageChooserDialogClass @ {:?}", self as *const _))
         .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellLanguageChooserInterface {
    pub parent_interface: gobject::GTypeInterface,
    pub get_language_full: Option<unsafe extern "C" fn(*mut GspellLanguageChooser, *mut gboolean) -> *const GspellLanguage>,
    pub set_language: Option<unsafe extern "C" fn(*mut GspellLanguageChooser, *const GspellLanguage)>,
}

impl ::std::fmt::Debug for GspellLanguageChooserInterface {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellLanguageChooserInterface @ {:?}", self as *const _))
         .field("parent_interface", &self.parent_interface)
         .field("get_language_full", &self.get_language_full)
         .field("set_language", &self.set_language)
         .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellNavigatorInterface {
    pub parent_interface: gobject::GTypeInterface,
    pub goto_next: Option<unsafe extern "C" fn(*mut GspellNavigator, *mut *mut c_char, *mut *mut GspellChecker, *mut *mut glib::GError) -> gboolean>,
    pub change: Option<unsafe extern "C" fn(*mut GspellNavigator, *const c_char, *const c_char)>,
    pub change_all: Option<unsafe extern "C" fn(*mut GspellNavigator, *const c_char, *const c_char)>,
}

impl ::std::fmt::Debug for GspellNavigatorInterface {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellNavigatorInterface @ {:?}", self as *const _))
         .field("parent_interface", &self.parent_interface)
         .field("goto_next", &self.goto_next)
         .field("change", &self.change)
         .field("change_all", &self.change_all)
         .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellNavigatorTextViewClass {
    pub parent_class: gobject::GInitiallyUnownedClass,
    pub padding: [gpointer; 8],
}

impl ::std::fmt::Debug for GspellNavigatorTextViewClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellNavigatorTextViewClass @ {:?}", self as *const _))
         .field("parent_class", &self.parent_class)
         .field("padding", &self.padding)
         .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellTextBufferClass {
    pub parent_class: gobject::GObjectClass,
}

impl ::std::fmt::Debug for GspellTextBufferClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellTextBufferClass @ {:?}", self as *const _))
         .field("parent_class", &self.parent_class)
         .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellTextViewClass {
    pub parent_class: gobject::GObjectClass,
    pub padding: [gpointer; 8],
}

impl ::std::fmt::Debug for GspellTextViewClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellTextViewClass @ {:?}", self as *const _))
         .field("parent_class", &self.parent_class)
         .field("padding", &self.padding)
         .finish()
    }
}

// Classes
#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellChecker {
    pub parent_instance: gobject::GObject,
}

impl ::std::fmt::Debug for GspellChecker {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellChecker @ {:?}", self as *const _))
         .field("parent_instance", &self.parent_instance)
         .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellCheckerDialog {
    pub parent_instance: gtk::GtkDialog,
}

impl ::std::fmt::Debug for GspellCheckerDialog {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellCheckerDialog @ {:?}", self as *const _))
         .field("parent_instance", &self.parent_instance)
         .finish()
    }
}

#[repr(C)]
pub struct GspellEntry(c_void);

impl ::std::fmt::Debug for GspellEntry {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellEntry @ {:?}", self as *const _))
         .finish()
    }
}

#[repr(C)]
pub struct GspellEntryBuffer(c_void);

impl ::std::fmt::Debug for GspellEntryBuffer {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellEntryBuffer @ {:?}", self as *const _))
         .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellLanguageChooserButton {
    pub parent_instance: gtk::GtkButton,
}

impl ::std::fmt::Debug for GspellLanguageChooserButton {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellLanguageChooserButton @ {:?}", self as *const _))
         .field("parent_instance", &self.parent_instance)
         .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellLanguageChooserDialog {
    pub parent_instance: gtk::GtkDialog,
}

impl ::std::fmt::Debug for GspellLanguageChooserDialog {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellLanguageChooserDialog @ {:?}", self as *const _))
         .field("parent_instance", &self.parent_instance)
         .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellNavigatorTextView {
    pub parent_instance: gobject::GInitiallyUnowned,
}

impl ::std::fmt::Debug for GspellNavigatorTextView {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellNavigatorTextView @ {:?}", self as *const _))
         .field("parent_instance", &self.parent_instance)
         .finish()
    }
}

#[repr(C)]
pub struct GspellTextBuffer(c_void);

impl ::std::fmt::Debug for GspellTextBuffer {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellTextBuffer @ {:?}", self as *const _))
         .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct GspellTextView {
    pub parent_instance: gobject::GObject,
}

impl ::std::fmt::Debug for GspellTextView {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("GspellTextView @ {:?}", self as *const _))
         .field("parent_instance", &self.parent_instance)
         .finish()
    }
}

// Interfaces
#[repr(C)]
pub struct GspellLanguageChooser(c_void);

impl ::std::fmt::Debug for GspellLanguageChooser {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "GspellLanguageChooser @ {:?}", self as *const _)
    }
}

#[repr(C)]
pub struct GspellNavigator(c_void);

impl ::std::fmt::Debug for GspellNavigator {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "GspellNavigator @ {:?}", self as *const _)
    }
}


extern "C" {

    //=========================================================================
    // GspellCheckerError
    //=========================================================================
    pub fn gspell_checker_error_get_type() -> GType;
    pub fn gspell_checker_error_quark() -> glib::GQuark;

    //=========================================================================
    // GspellLanguage
    //=========================================================================
    pub fn gspell_language_get_type() -> GType;
    pub fn gspell_language_compare(language_a: *const GspellLanguage, language_b: *const GspellLanguage) -> c_int;
    pub fn gspell_language_copy(language: *const GspellLanguage) -> *mut GspellLanguage;
    pub fn gspell_language_free(language: *mut GspellLanguage);
    pub fn gspell_language_get_code(language: *const GspellLanguage) -> *const c_char;
    pub fn gspell_language_get_name(language: *const GspellLanguage) -> *const c_char;
    pub fn gspell_language_get_available() -> *const glib::GList;
    pub fn gspell_language_get_default() -> *const GspellLanguage;
    pub fn gspell_language_lookup(language_code: *const c_char) -> *const GspellLanguage;

    //=========================================================================
    // GspellChecker
    //=========================================================================
    pub fn gspell_checker_get_type() -> GType;
    pub fn gspell_checker_new(language: *const GspellLanguage) -> *mut GspellChecker;
    pub fn gspell_checker_add_word_to_personal(checker: *mut GspellChecker, word: *const c_char, word_length: ssize_t);
    pub fn gspell_checker_add_word_to_session(checker: *mut GspellChecker, word: *const c_char, word_length: ssize_t);
    pub fn gspell_checker_check_word(checker: *mut GspellChecker, word: *const c_char, word_length: ssize_t, error: *mut *mut glib::GError) -> gboolean;
    pub fn gspell_checker_clear_session(checker: *mut GspellChecker);
    pub fn gspell_checker_get_enchant_dict(checker: *mut GspellChecker);
    pub fn gspell_checker_get_language(checker: *mut GspellChecker) -> *const GspellLanguage;
    pub fn gspell_checker_get_suggestions(checker: *mut GspellChecker, word: *const c_char, word_length: ssize_t) -> *mut glib::GSList;
    pub fn gspell_checker_set_correction(checker: *mut GspellChecker, word: *const c_char, word_length: ssize_t, replacement: *const c_char, replacement_length: ssize_t);
    pub fn gspell_checker_set_language(checker: *mut GspellChecker, language: *const GspellLanguage);

    //=========================================================================
    // GspellCheckerDialog
    //=========================================================================
    pub fn gspell_checker_dialog_get_type() -> GType;
    pub fn gspell_checker_dialog_new(parent: *mut gtk::GtkWindow, navigator: *mut GspellNavigator) -> *mut gtk::GtkWidget;
    pub fn gspell_checker_dialog_get_spell_navigator(dialog: *mut GspellCheckerDialog) -> *mut GspellNavigator;

    //=========================================================================
    // GspellEntry
    //=========================================================================
    pub fn gspell_entry_get_type() -> GType;
    pub fn gspell_entry_get_from_gtk_entry(gtk_entry: *mut gtk::GtkEntry) -> *mut GspellEntry;
    pub fn gspell_entry_basic_setup(gspell_entry: *mut GspellEntry);
    pub fn gspell_entry_get_entry(gspell_entry: *mut GspellEntry) -> *mut gtk::GtkEntry;
    pub fn gspell_entry_get_inline_spell_checking(gspell_entry: *mut GspellEntry) -> gboolean;
    pub fn gspell_entry_set_inline_spell_checking(gspell_entry: *mut GspellEntry, enable: gboolean);

    //=========================================================================
    // GspellEntryBuffer
    //=========================================================================
    pub fn gspell_entry_buffer_get_type() -> GType;
    pub fn gspell_entry_buffer_get_from_gtk_entry_buffer(gtk_buffer: *mut gtk::GtkEntryBuffer) -> *mut GspellEntryBuffer;
    pub fn gspell_entry_buffer_get_buffer(gspell_buffer: *mut GspellEntryBuffer) -> *mut gtk::GtkEntryBuffer;
    pub fn gspell_entry_buffer_get_spell_checker(gspell_buffer: *mut GspellEntryBuffer) -> *mut GspellChecker;
    pub fn gspell_entry_buffer_set_spell_checker(gspell_buffer: *mut GspellEntryBuffer, spell_checker: *mut GspellChecker);

    //=========================================================================
    // GspellLanguageChooserButton
    //=========================================================================
    pub fn gspell_language_chooser_button_get_type() -> GType;
    pub fn gspell_language_chooser_button_new(current_language: *const GspellLanguage) -> *mut gtk::GtkWidget;

    //=========================================================================
    // GspellLanguageChooserDialog
    //=========================================================================
    pub fn gspell_language_chooser_dialog_get_type() -> GType;
    pub fn gspell_language_chooser_dialog_new(parent: *mut gtk::GtkWindow, current_language: *const GspellLanguage, flags: gtk::GtkDialogFlags) -> *mut gtk::GtkWidget;

    //=========================================================================
    // GspellNavigatorTextView
    //=========================================================================
    pub fn gspell_navigator_text_view_get_type() -> GType;
    pub fn gspell_navigator_text_view_new(view: *mut gtk::GtkTextView) -> *mut GspellNavigator;
    pub fn gspell_navigator_text_view_get_view(navigator: *mut GspellNavigatorTextView) -> *mut gtk::GtkTextView;

    //=========================================================================
    // GspellTextBuffer
    //=========================================================================
    pub fn gspell_text_buffer_get_type() -> GType;
    pub fn gspell_text_buffer_get_from_gtk_text_buffer(gtk_buffer: *mut gtk::GtkTextBuffer) -> *mut GspellTextBuffer;
    pub fn gspell_text_buffer_get_buffer(gspell_buffer: *mut GspellTextBuffer) -> *mut gtk::GtkTextBuffer;
    pub fn gspell_text_buffer_get_spell_checker(gspell_buffer: *mut GspellTextBuffer) -> *mut GspellChecker;
    pub fn gspell_text_buffer_set_spell_checker(gspell_buffer: *mut GspellTextBuffer, spell_checker: *mut GspellChecker);

    //=========================================================================
    // GspellTextView
    //=========================================================================
    pub fn gspell_text_view_get_type() -> GType;
    pub fn gspell_text_view_get_from_gtk_text_view(gtk_view: *mut gtk::GtkTextView) -> *mut GspellTextView;
    pub fn gspell_text_view_basic_setup(gspell_view: *mut GspellTextView);
    pub fn gspell_text_view_get_enable_language_menu(gspell_view: *mut GspellTextView) -> gboolean;
    pub fn gspell_text_view_get_inline_spell_checking(gspell_view: *mut GspellTextView) -> gboolean;
    pub fn gspell_text_view_get_view(gspell_view: *mut GspellTextView) -> *mut gtk::GtkTextView;
    pub fn gspell_text_view_set_enable_language_menu(gspell_view: *mut GspellTextView, enable_language_menu: gboolean);
    pub fn gspell_text_view_set_inline_spell_checking(gspell_view: *mut GspellTextView, enable: gboolean);

    //=========================================================================
    // GspellLanguageChooser
    //=========================================================================
    pub fn gspell_language_chooser_get_type() -> GType;
    pub fn gspell_language_chooser_get_language(chooser: *mut GspellLanguageChooser) -> *const GspellLanguage;
    pub fn gspell_language_chooser_get_language_code(chooser: *mut GspellLanguageChooser) -> *const c_char;
    pub fn gspell_language_chooser_set_language(chooser: *mut GspellLanguageChooser, language: *const GspellLanguage);
    pub fn gspell_language_chooser_set_language_code(chooser: *mut GspellLanguageChooser, language_code: *const c_char);

    //=========================================================================
    // GspellNavigator
    //=========================================================================
    pub fn gspell_navigator_get_type() -> GType;
    pub fn gspell_navigator_change(navigator: *mut GspellNavigator, word: *const c_char, change_to: *const c_char);
    pub fn gspell_navigator_change_all(navigator: *mut GspellNavigator, word: *const c_char, change_to: *const c_char);
    pub fn gspell_navigator_goto_next(navigator: *mut GspellNavigator, word: *mut *mut c_char, spell_checker: *mut *mut GspellChecker, error: *mut *mut glib::GError) -> gboolean;

}
